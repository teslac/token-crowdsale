import React from 'react'
import ReactCountdownClock from 'react-countdown-clock'

import { withNamespaces } from 'react-i18next';

class CountdownTimer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {
      displaySeconds,
      nextTick,
      tiersLength,
      days,
      hours,
      minutes,
      seconds,
      msToNextTick,
      onComplete,
      isFinalized,
      t
    } = this.props

    const countdownClock = (
      <div>
        {displaySeconds
          ? null
          : <div className="timer-i">
            <div className="timer-count">{days}</div>
            <div className="timer-interval">{t("days")}</div>
          </div>
        }
        {displaySeconds
          ? null
          : <div className="timer-i">
            <div className="timer-count">{hours}</div>
            <div className="timer-interval">{t("hours")}</div>
          </div>
        }
        <div className="timer-i">
          <div className="timer-count">{minutes}</div>
          <div className="timer-interval">{t("mins")}</div>
        </div>
        {!displaySeconds
          ? null
          : <div className="timer-i">
            <div className="timer-count">{seconds}</div>
            <div className="timer-interval">{t("secs")}</div>
          </div>
        }
      </div>
    )

    let message = null

    if (isFinalized) {
      message = t("finalized")
    } else {
      if (nextTick.type) {
        if (nextTick.type === 'start') {
          message = t("to_start") + nextTick.order//`to start of tier ${nextTick.order} of ${tiersLength}`
        } else {
          message = t("to_end") + nextTick.order//`to end of tier ${nextTick.order} of ${tiersLength}`
        }
      } else {
        message = 'crowdsale has ended'
      }
    }

    return (
      <div className="timer-container">
        <div style={{ marginLeft: '-20px', marginTop: '-20px' }}>
          <ReactCountdownClock
            seconds={msToNextTick / 1000}
            color="#733EAB"
            alpha={0.9}
            size={270}
            showMilliseconds={false}
            onComplete={onComplete}
          />
        </div>
        <div className="timer">
          <div className="timer-inner">
            {isFinalized ? null : countdownClock}
            <div className="timer-i">
              <div className="timer-interval">
                <strong>
                  {message}
                </strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default withNamespaces()(CountdownTimer)
