import React from 'react'
import '../../assets/stylesheets/application.css';
import { Link } from 'react-router-dom'
import moment from "moment";
import { displayHeaderAndFooterInIframe } from '../../utils/utils'

import { withNamespaces } from 'react-i18next';
class Footer extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const displayFooter = displayHeaderAndFooterInIframe()

    const { t } = this.props
    const footer = (
      <footer className="footer">
        <div className="container">
          <p className="rights">{moment().format('YYYY')} {t("copyright")}</p>
          <Link className="logo" to='/' />
          <div className="socials">
            <a href="https://ludos.one" className="social social_oracles">Ludos Protocol</a>
            <a href="https://twitter.com/ludosprotocol" className="social social_twitter">Twitter</a>
            <a href="https://t.me/ludosprotocol" className="social social_telegram">Telegram</a>
            <a href="https://github.com/Ludos-One" className="social social_github">GitHub</a>
          </div>
        </div>
      </footer>
    )

    return displayFooter ? footer : null
  }
}
export default withNamespaces()(Footer)
