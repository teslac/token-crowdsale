import React from 'react'
import { FormSpy } from 'react-final-form'
import { TokenName } from '../Common/TokenName'
import { TokenAddress } from '../Common/TokenAddress'
import { TokenTicker } from '../Common/TokenTicker'
import { TokenDecimals } from '../Common/TokenDecimals'
import { ReservedTokensInputBlock } from '../Common/ReservedTokensInputBlock'

const errorStyle = {
  color: 'red',
  fontWeight: 'bold',
  fontSize: '12px',
  width: '100%',
  height: '20px',
}

export const StepTwoForm = ({
  id,
  handleSubmit,
  disableDecimals,
  updateTokenStore,
  tokens,
  decimals,
  addReservedTokensItem,
  removeReservedToken,
  clearAll
}) => {
  return (
    <form id={id} onSubmit={handleSubmit}>
      <div className="reserved-tokens-title">
        <p className="title">Existing Token</p>
      </div>
      <div className="hidden">
        <TokenAddress errorStyle={errorStyle} />
        <TokenName errorStyle={errorStyle} />
        <TokenDecimals disabled={disableDecimals} errorStyle={errorStyle} />
        <TokenTicker errorStyle={errorStyle} />
      </div>
      <div className="button-container">
        <a onClick={e => { e.preventDefault(); handleSubmit() }} className="button button_fill">Continue</a>
      </div>

      <FormSpy onChange={updateTokenStore} />
    </form>
  )
}
