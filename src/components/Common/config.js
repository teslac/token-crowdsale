// DEMO CONFIG (DO NOT DELETE THIS EXAMPLE):
const networks = {
  mainnet: 1,
  morden: 2,
  ropsten: 3,
  rinkeby: 4,
  kovan: 42,
  sokolPOA: 77,
  corePOA: 99
}

export const CrowdsaleConfig = {
  crowdsaleContractURL: '0xdc12a8E62a778D0732e3F4b522a733504EE2D369',
  networkID: networks.mainnet,
  showHeaderAndFooterInIframe: true,
  tokenSymbol: 'LUD',
  tokenDescription: 'Ludos Protocol is a decentralized solution for game ecosystem.'
};

// export const CrowdsaleConfig = {};
