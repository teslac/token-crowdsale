import React from 'react'
import '../../assets/stylesheets/application.css';

import i18n from '../../i18n';

export const Lang = () => {
  let langs = {en:"English", ja:"日本語", zh:"繁體中文"};
  return <div style={{zIndex:'10',position:'relative',textAlign:'right'}}>
    <select
    className="invest-through"
    style={{marginLeft:'10px', float:'right'}}
    onChange={(event) => {
      i18n.changeLanguage(event.target.value);
    }}>
        {Object.keys(langs).map(locale => (
            <option
                value={locale}
            >
                {langs[locale]}
            </option>
        ))}

    </select>
    </div>
}