import React from 'react'
import { composeValidators, isRequired, isMaxLength, isMatchingPattern } from '../../utils/validations'
import { TEXT_FIELDS } from '../../utils/constants'
import { Field } from 'react-final-form'
import { InputField2 } from './InputField2'

export const TokenAddress = ({ errorStyle }) => (
  <Field
    validate={(value) => {
      const errors = composeValidators(
        isRequired(),
        isMaxLength()(42),
        isMatchingPattern('Name should have at least one character')(/.*\S.*/)
      )(value)

      if (errors) return errors.shift()
    }}
    component={InputField2}
    side="left"
    name="address"
    type="text"
    description="The address of the existing token. It should be start with '0x', and the length is 42."
    label={TEXT_FIELDS.ADDRESS}
    errorStyle={errorStyle}
  />
)
