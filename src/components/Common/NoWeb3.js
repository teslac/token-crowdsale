import React, { Component } from 'react';
import { withNamespaces } from 'react-i18next';

class NoWeb3 extends Component {
  render () {
    const { t } = this.props
    return (
      <div>
        <section className="home">
          <div className="crowdsale">
            <div className="container">
              <h1 className="title">{t("no_wallet_title")}</h1>
              <p className="description">
                {t("no_wallet_desc1")}
                {t("no_wallet_desc2")}<a href={t("no_wallet_desc_url")} target='blank'>{t("no_wallet_desc3")}</a>.
              </p>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withNamespaces()(NoWeb3)