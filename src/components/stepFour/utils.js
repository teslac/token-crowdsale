import {
  attachToContract,
  calculateGasLimit,
  deployContract,
  getNetWorkNameById,
  getNetworkVersion,
  getRegistryAddress,
  sendTXToContract
} from '../../utils/blockchainHelpers'
import { noContractAlert, noContractDataAlert } from '../../utils/alerts'
import { countDecimalPlaces, toFixed } from '../../utils/utils'
import { DOWNLOAD_NAME } from '../../utils/constants'
import { isObservableArray } from 'mobx'
import {
  contractStore,
  deploymentStore,
  generalStore,
  reservedTokenStore,
  tierStore,
  tokenStore,
  web3Store
} from '../../stores'
import { getEncodedABIClientSide } from '../../utils/microservices'
import { BigNumber } from 'bignumber.js'
import { toBigNumber } from '../crowdsale/utils'

export const setupContractDeployment = (web3) => {
  if (!contractStore.safeMathLib) {
    noContractDataAlert()
    return Promise.reject('no contract data')
  }

  const tokenABI = contractStore.token.abi || []
  const tokenAddr = contractStore.token.addr || null
  const pricingStrategyABI = contractStore.pricingStrategy.abi || []

  const whenTokenABIConstructor = Promise.resolve(tokenAddr)
    .then(tokenAddr => {
      if (!tokenAddr) {
        return getEncodedABIClientSide(web3, tokenABI, [], 0)
          .then(ABIEncoded => {
            console.log('token ABI Encoded params constructor:', ABIEncoded)
            contractStore.setContractProperty('token', 'abiConstructor', ABIEncoded)
          })
      }
    })

  const whenPricingStrategyContract = tierStore.tiers.map((value, index) => {
    return getEncodedABIClientSide(web3, pricingStrategyABI, [], index)
      .then(ABIEncoded => {
        console.log('pricingStrategy ABI Encoded params constructor:', ABIEncoded)
        const newContract = contractStore.pricingStrategy.abiConstructor.concat(ABIEncoded)
        contractStore.setContractProperty('pricingStrategy', 'abiConstructor', newContract)
      })
  })

  return Promise.all([whenTokenABIConstructor, ...whenPricingStrategyContract])
}

export const buildDeploymentSteps = (web3) => {
  const stepFnCorrelation = {
    safeMathLibrary: deploySafeMathLibrary,
    token: deployToken,
    pricingStrategy: deployPricingStrategy(web3),
    crowdsale: deployCrowdsale,
    registerCrowdsaleAddress: registerCrowdsaleAddress,
    // finalizeAgent: deployFinalizeAgent(web3),
    tier: setTier,
    // setReservedTokens: setReservedTokensListMultiple,
    updateJoinedCrowdsales: updateJoinedCrowdsales,
    // setMintAgentCrowdsale: setMintAgentForCrowdsale,
    // setMintAgentFinalizeAgent: setMintAgentForFinalizeAgent,
    whitelist: addWhitelist,
    // setFinalizeAgent: setFinalizeAgent,
    // setReleaseAgent: setReleaseAgent,
    // transferOwnership: transferOwnership,
  }

  let list = []

  deploymentStore.txMap.forEach((steps, name) => {
    if (steps.length) {
      list = list.concat(stepFnCorrelation[name]())
    }
  })

  return list
}

export const deploySafeMathLibrary = () => {
  // return [
  //   () => {
  //     deploymentStore.setAsSuccessful('safeMathLibrary')
  //   }
  // ]
  return [
    () => {
      const binSafeMathLib = contractStore.safeMathLib.bin || ''
      const abiSafeMathLib = contractStore.safeMathLib.abi || []

      console.log('***Deploy safeMathLib contract***')

      return deployContract(abiSafeMathLib, binSafeMathLib, [])
        .then(safeMathLibAddr => {
          contractStore.setContractProperty('safeMathLib', 'addr', safeMathLibAddr)

          try {
            Object.keys(contractStore)
              .filter(key => contractStore[key] !== undefined)
              .forEach(key => {
                if (contractStore[key].bin) {
                  const strToReplace = '__:SafeMathLibExt_______________________'
                  const newBin = window.reaplaceAll(strToReplace, safeMathLibAddr.substr(2), contractStore[key].bin)
                  contractStore.setContractProperty(key, 'bin', newBin)
                }
              })
            deploymentStore.setAsSuccessful('safeMathLibrary')
            return Promise.resolve()

          } catch (e) {
            return Promise.reject(e)
          }
        })
    }
  ]
}

export const deployToken = () => {
  return [
    () => {
      contractStore.setContractProperty('token', 'addr', tokenStore.address)
      deploymentStore.setAsSuccessful('token')
    }
  ]
}

const getPricingStrategyParams = tier => {
  BigNumber.config({ DECIMAL_PLACES: 18 })
  const rate = new BigNumber(tier.rate)
  const oneTokenInETH = rate.pow(-1).toFixed()

  return [
    web3Store.web3.utils.toWei(oneTokenInETH, 'ether')
  ]
}

export const deployPricingStrategy = (web3) => () => {
  return tierStore.tiers.map((tier, index) => {
    return () => {
      const abiPricingStrategy = contractStore.pricingStrategy.abi || []
      const binPricingStrategy = contractStore.pricingStrategy.bin || ''
      const abiCrowdsale = contractStore.crowdsale.abi || []

      const paramsPricingStrategy = getPricingStrategyParams(tier)

      return deployContract(abiPricingStrategy, binPricingStrategy, paramsPricingStrategy)
        .then(pricingStrategyAddr => contractStore.pricingStrategy.addr.concat(pricingStrategyAddr))
        .then(newPricingStrategy => contractStore.setContractProperty('pricingStrategy', 'addr', newPricingStrategy))
        .then(() => getEncodedABIClientSide(web3, abiCrowdsale, [], index, true))
        .then(ABIEncoded => contractStore.crowdsale.abiConstructor.concat(ABIEncoded))
        .then(newContract => contractStore.setContractProperty('crowdsale', 'abiConstructor', newContract))
        .then(() => deploymentStore.setAsSuccessful('pricingStrategy'))
    }
  })
}

const getCrowdSaleParams = index => {
  const { walletAddress, whitelistEnabled } = tierStore.tiers[0]
  const { updatable, supply, tier, startTime, endTime } = tierStore.tiers[index]

  const formatDate = date => toFixed(parseInt(Date.parse(date) / 1000, 10).toString())

  return [
    tier,
    contractStore.token.addr,
    contractStore.pricingStrategy.addr[index],
    walletAddress,
    formatDate(startTime),
    formatDate(endTime),
    toFixed('0'),
    toBigNumber(supply).times(`1e${tokenStore.decimals}`).toFixed(),
    updatable === 'on',
    whitelistEnabled === 'yes'
  ]
}

export const deployCrowdsale = () => {
  return tierStore.tiers.map((tier, index) => {
    return () => {
      return getNetworkVersion()
        .then(networkID => contractStore.setContractProperty('crowdsale', 'networkID', networkID))
        .then(() => {
          const abiCrowdsale = contractStore.crowdsale.abi || []
          const binCrowdsale = contractStore.crowdsale.bin || ''
          const paramsCrowdsale = getCrowdSaleParams(index)

          return deployContract(abiCrowdsale, binCrowdsale, paramsCrowdsale)
        })
        .then(crowdsaleAddr => {
          console.log('***Deploy crowdsale contract***', index, crowdsaleAddr)

          const newCrowdsaleAddr = contractStore.crowdsale.addr.concat(crowdsaleAddr)
          contractStore.setContractProperty('crowdsale', 'addr', newCrowdsaleAddr)
        })
        .then(() => deploymentStore.setAsSuccessful('crowdsale'))
    }
  })
}

function registerCrowdsaleAddress () {
  return [
    () => {
      const { web3 } = web3Store
      const toJS = x => JSON.parse(JSON.stringify(x))

      const registryAbi = contractStore.registry.abi
      const crowdsaleAddress = contractStore.crowdsale.addr[0]

      const whenRegistryAddress = getRegistryAddress()

      const whenAccount = web3.eth.getAccounts()
        .then((accounts) => accounts[0])

      return Promise.all([whenRegistryAddress, whenAccount])
        .then(([registryAddress, account]) => {
          const registry = new web3.eth.Contract(toJS(registryAbi), registryAddress)

          const opts = { gasPrice: generalStore.gasPrice, from: account }
          const method = registry.methods.add(crowdsaleAddress)

          return method.estimateGas(opts)
            .then(estimatedGas => {
              opts.gasLimit = calculateGasLimit(estimatedGas)
              return sendTXToContract(method.send(opts))
            })
        })
        .then(() => deploymentStore.setAsSuccessful('registerCrowdsaleAddress'))
    }
  ]
}

export const deployFinalizeAgent = (web3) => () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('finalizeAgent')
    }
  ]
}

export const setTier = () => {
  return tierStore.tiers.map((tier, index) => {
    return () => {
      console.log('###setTier for Pricing Strategy:###')

      const pricingStrategyAddr = contractStore.pricingStrategy.addr[index]
      const pricingStrategyABI = contractStore.pricingStrategy.abi.slice()
      const tierAddr = contractStore.crowdsale.addr[index]

      return attachToContract(pricingStrategyABI, pricingStrategyAddr)
        .then(pricingStrategyContract => {
          console.log('attach to pricingStrategy contract')

          const opts = { gasPrice: generalStore.gasPrice }
          const method = pricingStrategyContract.methods.setTier(tierAddr)

          return method.estimateGas(opts)
            .then(estimatedGas => {
              opts.gasLimit = calculateGasLimit(estimatedGas)
              return sendTXToContract(method.send(opts))
            })
        })
        .then(() => deploymentStore.setAsSuccessful('tier'))
    }
  })

}

export const setMintAgentForCrowdsale = () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('setMintAgentCrowdsale')
    }
  ]
}

export const setMintAgentForFinalizeAgent = () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('setMintAgentFinalizeAgent')
    }
  ]
}

export const addWhitelist = () => {
  return tierStore.tiers.map((tier, index) => {
    return () => {
      const round = index
      const abi = contractStore.crowdsale.abi.slice()
      const addr = contractStore.crowdsale.addr[index]

      console.log('###whitelist:###')
      let whitelist = []

      for (let i = 0; i <= round; i++) {
        const tier = tierStore.tiers[i]

        for (let j = 0; j < tier.whitelist.length; j++) {
          let itemIsAdded = false

          for (let k = 0; k < whitelist.length; k++) {
            if (whitelist[k].addr === tier.whitelist[j].addr) {
              itemIsAdded = true
              break
            }
          }

          if (!itemIsAdded) {
            whitelist.push.apply(whitelist, tier.whitelist)
          }
        }
      }

      console.log('whitelist:', whitelist)

      if (whitelist.length === 0) {
        return Promise.resolve()
      }

      return attachToContract(abi, addr)
        .then(crowdsaleContract => {
          console.log('attach to crowdsale contract')

          if (!crowdsaleContract) {
            noContractAlert()
            return Promise.reject('No contract available')
          }

          let addrs = []
          let statuses = []
          let minCaps = []
          let maxCaps = []

          for (let i = 0; i < whitelist.length; i++) {
            addrs.push(whitelist[i].addr)
            statuses.push(true)
            minCaps.push(whitelist[i].min * 10 ** tokenStore.decimals ? toFixed((whitelist[i].min * 10 ** tokenStore.decimals).toString()) : 0)
            maxCaps.push(whitelist[i].max * 10 ** tokenStore.decimals ? toFixed((whitelist[i].max * 10 ** tokenStore.decimals).toString()) : 0)
          }

          console.log('addrs:', addrs)
          console.log('statuses:', minCaps)
          console.log('maxCaps:', maxCaps)

          const opts = { gasPrice: generalStore.gasPrice }
          const method = crowdsaleContract.methods.setEarlyParticipantWhitelistMultiple(addrs, statuses, minCaps, maxCaps)

          return method.estimateGas(opts)
            .then(estimatedGas => {
              opts.gasLimit = calculateGasLimit(estimatedGas)
              return sendTXToContract(method.send(opts))
            })
        })
        .then(() => deploymentStore.setAsSuccessful('whitelist'))
    }

  })
}

export const updateJoinedCrowdsales = () => {
  return tierStore.tiers.map((tier, index) => {
    return () => {
      const joinedContractAddresses = contractStore.crowdsale.addr
      const abi = contractStore.crowdsale.abi.slice()

      console.log('###updateJoinedCrowdsales:###')

      return attachToContract(abi, joinedContractAddresses[index])
        .then(crowdsaleContract => {
          console.log('attach to crowdsale contract')

          if (!crowdsaleContract) {
            noContractAlert()
            return Promise.reject('no contract available')
          }

          let addrs = []

          for (let i = 0; i < joinedContractAddresses.length; i++) {
            addrs.push(joinedContractAddresses[i])
          }

          console.log('input:', joinedContractAddresses)
          console.log('input2:', addrs)

          const opts = { gasPrice: generalStore.gasPrice }
          const method = crowdsaleContract.methods.updateJoinedCrowdsalesMultiple(addrs)

          return method.estimateGas(opts)
            .then(estimatedGas => {
              opts.gasLimit = calculateGasLimit(estimatedGas)
              return sendTXToContract(method.send(opts))
            })
        })
        .then(() => deploymentStore.setAsSuccessful('updateJoinedCrowdsales'))
    }
  })
}

export const setFinalizeAgent = () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('setFinalizeAgent')
    }
  ]
}

export const setReleaseAgent = () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('setReleaseAgent')
    }
  ]
}

export const setReservedTokensListMultiple = () => {
  return [() => {
    const abi = contractStore.token.abi.slice()
    const addr = contractStore.token.addr

    console.log('###setReservedTokensListMultiple:###')

    return attachToContract(abi, addr)
      .then(tokenContract => {
        console.log('attach to token contract')

        if (!tokenContract) {
          noContractAlert()
          return Promise.reject('no contract available')
        }

        let map = {}
        let addrs = []
        let inTokens = []
        let inPercentageUnit = []
        let inPercentageDecimals = []

        const reservedTokens = reservedTokenStore.tokens

        for (let i = 0; i < reservedTokens.length; i++) {
          if (!reservedTokens[i].deleted) {
            const val = reservedTokens[i].val
            const addr = reservedTokens[i].addr
            const obj = map[addr] ? map[addr] : {}

            if (reservedTokens[i].dim === 'tokens') {
              obj.inTokens = val * 10 ** tokenStore.decimals
            } else {
              obj.inPercentageDecimals = countDecimalPlaces(val)
              obj.inPercentageUnit = val * 10 ** obj.inPercentageDecimals
            }
            map[addr] = obj
          }
        }

        let keys = Object.keys(map)

        for (let i = 0; i < keys.length; i++) {
          let key = keys[i]
          let obj = map[key]

          addrs.push(key)
          inTokens.push(obj.inTokens ? toFixed(obj.inTokens.toString()) : 0)
          inPercentageUnit.push(obj.inPercentageUnit ? obj.inPercentageUnit : 0)
          inPercentageDecimals.push(obj.inPercentageDecimals ? obj.inPercentageDecimals : 0)
        }

        if (addrs.length === 0 && inTokens.length === 0 && inPercentageUnit.length === 0) {
          if (inPercentageDecimals.length === 0) return Promise.resolve()
        }

        const opts = { gasPrice: generalStore.gasPrice }
        const method = tokenContract.methods
          .setReservedTokensListMultiple(addrs, inTokens, inPercentageUnit, inPercentageDecimals)

        return method.estimateGas(opts)
          .then(estimatedGas => {
            opts.gasLimit = calculateGasLimit(estimatedGas)
            return sendTXToContract(method.send(opts))
          })
      })
      .then(() => deploymentStore.setAsSuccessful('setReservedTokens'))
  }]
}

export const transferOwnership = () => {
  return [
    () => {
      deploymentStore.setAsSuccessful('transferOwnership')
    }
  ]
}

export const handlerForFile = (content, type) => {
  const checkIfTime = content.field === 'startTime' || content.field === 'endTime'
  let suffix = ''

  if (checkIfTime) {
    let timezoneOffset = (new Date()).getTimezoneOffset() / 60
    let operator = timezoneOffset > 0 ? '-' : '+'
    suffix = ` (GMT ${operator} ${Math.abs(timezoneOffset)})`
  }

  console.log("content:", content)
  console.log("type:", type)

  if (content && type) {
    return `${content.value}${type[content.field]}${suffix}`
  } else {
    if (!content) {
      console.log("WARNING!: content is undefined")
    }
    if (!type) {
      console.log("WARNING!: type is undefined")
    }
    return ''
  }
}

export const handleConstantForFile = content => {
  return `${content.value}${content.fileValue}`
}

export const handleContractsForFile = (content, index, contractStore, tierStore) => {
  const title = content.value
  const { field } = content
  let fileContent = ''

  if (field !== 'src' && field !== 'abi' && field !== 'addr') {
    const contractField = contractStore[content.child][field]
    let fileBody

    if (isObservableArray(contractField)) {
      fileBody = contractField[index]

      if (!!fileBody) {
        let tierName = tierStore.tiers[index] ? tierStore.tiers[index].tier : ''
        fileContent = `${title} for ${tierName}:**** \n\n${fileBody}`
      }
    } else if (!!contractField) {
      fileContent = title + ':**** \n\n' + contractField
    }
  } else {
    fileContent = addSrcToFile(content, index, contractStore, tierStore)
  }

  return fileContent
}

const addSrcToFile = (content, index, contractStore, tierStore) => {
  const title = content.value
  const { field } = content
  const contractField = contractStore[content.child][field]
  let fileContent = ''

  if (isObservableArray(contractField) && field !== 'abi') {
    let tierName = tierStore.tiers[index] ? tierStore.tiers[index].tier : ''
    fileContent = `${title} for ${tierName}: ${contractField[index]}`
  } else {
    if (field !== 'src') {
      const body = field === 'abi' ? JSON.stringify(contractField) : contractField
      fileContent = title + body
    } else {
      fileContent = contractField
    }
  }

  return fileContent
}

export const download = ({ data = {}, filename = '', type = '', zip = '' }) => {
  let file = !zip ? new Blob([data], { type: type }) : zip

  if (window.navigator.msSaveOrOpenBlob) { // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename)
  } else { // Others
    let a = document.createElement('a')
    let url = URL.createObjectURL(file)

    a.href = url
    a.download = filename
    document.body.appendChild(a)
    a.click()

    setTimeout(function () {
      document.body.removeChild(a)
      window.URL.revokeObjectURL(url)
    }, 0)
  }
}

export function scrollToBottom () {
  window.scrollTo(0, document.body.scrollHeight)
}

export function getDownloadName (tokenAddress) {
  return new Promise(resolve => {
    const whenNetworkName = getNetworkVersion()
      .then((networkId) => {
        let networkName = getNetWorkNameById(networkId)

        if (!networkName) {
          networkName = String(networkId)
        }

        return networkName
      })
      .then((networkName) => `${DOWNLOAD_NAME}_${networkName}_${tokenAddress}`)

    resolve(whenNetworkName)
  })
}
