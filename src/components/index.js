export { default as Footer } from './Footer';
export { Header } from './Header';
export { Home } from './Home';
export { stepOne } from './stepOne'
export { stepTwo } from './stepTwo'
export { stepThree } from './stepThree'
export { stepFour } from './stepFour'
export { default as Crowdsale } from './crowdsale'
export { default as Invest } from './invest'
export { Manage } from './manage'
export { Stats } from './stats'
